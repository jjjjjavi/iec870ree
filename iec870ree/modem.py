import time
import logging
from .protocol import PhysicalLayer, PhysicalLayerTimeout
from .utils import SerialPort, PortReader
import queue

logger = logging.getLogger('iec870ree.modem')


class ModemException(Exception):
    pass


class ModemState:
    def connect(self, modem):
        raise ModemException("no se puede en este estado")

    def disconnect(self, modem):
        raise ModemException("no se puede en este estado {}".format(self))

    def get_byte(self, modem, timeout):
        raise ModemException("no se puede en este estado")

    def send_byte(self, modem, bt):
        raise ModemException("no se puede en este estado {}".format(self))

    def set_state(self, modem, new_state):
        modem.set_state(new_state)


class DisconnectedState(ModemState):

    def connect(self, modem):
        modem.connect_port()
        self.set_state(modem, CommandState())
        # Para inicializar en CommandState. Si quisiéramos
        # utilizar esta clase para SMS u otra cosa, esto nos sobra
        # y habría que usarlo para otra cosa
        modem.connect()


class CommandState(ModemState):
    CONNECTED_MESSAGES = ["CONNECT", "REL ASYNC"]

    def connect(self, modem):
        self.initialize_modem(modem)

    def initialize_modem(self, modem):
        # leo todo lo que pueda haber en el puerto.
        line = self.get_line(modem)
        while line:
            line = self.get_line(modem)
            logger.info("got old message> " + line)
        self.call_phone(modem)
        self.waitforconnect(modem)

    def call_phone(self, modem):
        self.writeat(modem, "ATZ")
        time.sleep(2)  # at least two seconds after ATZ (reset) command
        self.writeat(modem, "AT+CBST=7,0,1")
        time.sleep(3)
        self.writeat(modem, "ATD" + str(modem.phone_number))

    def waitforconnect(self, modem):
        """ podriamos vigilar mensajes como 'BUSY', que el numero
        al que llamamos está ocupado o 'NO CARRIER' y así fallar
        antes y ahorrar tiempo."""
        max_waits = 40
        connected = False
        for i in range(max_waits):
            line = self.get_line(modem)
            while line and not connected:
                logger.info("got message> " + line)
                for message in CommandState.CONNECTED_MESSAGES:
                    if message in line:
                        logger.info("CONNECTED!!!!!!!!!!!!!!!!!!!!")
                        connected = True
                if 'NO CARRIER' in line:
                    raise ModemException("NO CARRIER")
                if 'BUSY' in line:
                    raise ModemException("BUSY")
                if not connected:
                    line = self.get_line(modem)
            if connected:
                break

            time.sleep(1)
        else:
            raise ModemException("No consigo conectar al número")

        time.sleep(0.5)
        self.set_state(modem, DataState())

    def get_line(self, modem):
        buffer = bytearray()
        try:
            while True:
                byte_read = modem.queue.get(False)
                modem.queue.task_done()
                if (byte_read == 0x0A):
                    resp = buffer.decode("ascii")
                    del buffer[:]
                    return resp
                else:
                    buffer.append(byte_read)
        except queue.Empty:
            pass

    def hang_up(self, modem):
        time.sleep(1)
        logger.info("envio +++")
        modem.port.send_bytes("+++".encode("ascii"))
        time.sleep(1)
        self.writeat(modem, "ATH0")
        time.sleep(1)

    def disconnect(self, modem):
        # FIXME presupongo que esta en estado data. Podría no estarlo.
        try:
            self.hang_up(modem)
        finally:
            modem.disconnect_port()
            self.set_state(modem, DisconnectedState())

    def writeat(self, modem, value):
        logger.info("sending atcommand>" + value)
        to_write = (value + "\r").encode("ascii")
        modem.port.send_bytes(to_write)


class DataState(ModemState):

    def disconnect(self, modem):
        self.set_state(modem, CommandState())
        modem.disconnect()

    def get_byte(self, modem, timeout):
        resp = modem.queue.get(True, timeout)
        return resp

    def send_byte(self, modem, bt):
        modem.port.send_byte(bt)


class Modem(PhysicalLayer):

    def __init__(self, phone_number, serial_port='/dev/ttyUSB0'):
        self.serial_port = serial_port
        self.phone_number = phone_number
        self.state = DisconnectedState()
        self.queue = queue.Queue()
        self.port = None

    def set_state(self, new_state):
        self.state = new_state

    def connect_port(self):
        self.port = SerialPort(self.serial_port,
                               baudrate=9600, timeout=1)
        self.port.connect()
        self.read_thread = PortReader(self.queue)
        self.read_thread.port = self.port
        self.read_thread.start()
        # yield, para que vaya el hilo y empiece
        time.sleep(0.5)

    def disconnect_port(self):
        self.read_thread.stop()
        self.read_thread.join(5)
        self.port.disconnect()

    def connect(self):
        self.state.connect(self)

    def disconnect(self):
        self.state.disconnect(self)

    def get_byte(self, timeout=30):
        try:
            return self.state.get_byte(self, timeout)
        except queue.Empty:
            raise PhysicalLayerTimeout()

    def send_byte(self, bt):
        return self.state.send_byte(self, bt)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.disconnect()
