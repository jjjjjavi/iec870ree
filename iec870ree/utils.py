import threading
import logging
import socket
import serial
logger = logging.getLogger('iec870ree.utils')


class Port():

    def connect(self):
        raise NotImplementedError

    def get_byte(self):
        raise NotImplementedError

    def send_byte(self, bt):
        raise NotImplementedError

    def disconnect(self):
        raise NotImplementedError

    def send_bytes(self, bts):
        for bt in bts:
            self.send_byte(bt)


class SerialPort(Port):
    def __init__(self, dev_name,
                 baudrate=9600, timeout=1):
        self.dev_name = dev_name
        self.baudrate = baudrate
        self.timeout = timeout

    def connect(self):
        self.port = serial.Serial(self.dev_name,
                                  baudrate=self.baudrate,
                                  timeout=self.timeout)

    def get_byte(self, ):
        response = self.port.read(1)
        if response is not None and len(response) > 0:
            return response[0]

    def send_byte(self, bt):
        self.port.write(bytes([bt]))

    def disconnect(self):
        self.port.close()


class SocketPort(Port):

    def __init__(self, addr):
        """ addr es una tupla (ip, port) """
        self.addr = addr

    def connect(self):
        self.socket = socket.create_connection(self.addr)
        self.socket.settimeout(2)
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    def get_byte(self, ):
        try:
            response = self.socket.recv(1)
        except socket.timeout:
            response = None
        if response is not None and len(response) > 0:
            return response[0]

    def send_byte(self, bt):
        self.socket.send(bytes([bt]))

    def disconnect(self):
        self.socket.close()


class PortReader(threading.Thread):

    def __init__(self, queue,):
        super().__init__()
        self.queue = queue
        self.port = None

    def stop(self):
        self.parar = True

    def run(self):
        logger.info("read thread Starting")
        self.parar = False

        while not self.parar:
            logger.debug("iterate read thread")
            response = self.port.get_byte()
            if response is None:
                continue
            self.queue.put(response)

        logger.info("read thread END")
