import logging
from abc import ABCMeta, abstractmethod
from .base_asdu import (
    FrameParser, FixedFrame, VariableFrame
)
import math


logger = logging.getLogger('iec870ree.protocol')


class ProtocolException(Exception):
    pass


class Nack(Exception):
    pass


class PhysicalLayerTimeout(Exception):
    pass


class AppLayer(metaclass=ABCMeta):

    def initialize(self, link_layer):
        self.link_layer = link_layer

    def ejecutar_send_confirm(self, user_data_asdu, registro):
        send_asdu = self.create_asdu_send(user_data_asdu, registro)
        self.link_layer.send_frame(send_asdu)
        asdu_ack = self.link_layer.get_frame()
        if not asdu_ack:
            raise ProtocolException("Didn't get ACK")
        # codigo de funcion tiene que ser 0. Un 1 es nack
        if asdu_ack.c.cf != 0:
            raise Nack("NACK: no hay confirm en send")
        return asdu_ack

    def ejecutar_request_response(self):
        asdu_req = self.create_asdu_request()
        self.link_layer.send_frame(asdu_req)
        asdu_resp = self.link_layer.get_frame()
        if asdu_resp.c.cf == 9:
            raise Nack("NACK: no hay datos en response")
        if asdu_resp.c.cf != 8:
            raise ProtocolException("CF invalida en Response")
        return asdu_resp

    def create_asdu_request(self):
        asdu = FixedFrame()
        asdu.c.res = 0
        asdu.c.prm = 1
        asdu.c.fcb = self.link_layer.fcb
        asdu.c.fcv = 1
        asdu.c.cf = 11
        asdu.der = self.link_layer.der
        asdu.generate()
        return asdu

    def create_asdu_send(self, user_data, registro):
        asdu = VariableFrame()
        asdu.c.res = 0
        asdu.c.prm = 1
        asdu.c.fcb = self.link_layer.fcb
        asdu.c.fcv = 1
        asdu.c.cf = 3
        asdu.der = self.link_layer.der
        asdu.cualificador_ev = math.ceil(
            getattr(user_data, 'data_length', 0x06)/0x06)
        asdu.causa_tm = getattr(user_data, 'causa_tm', 6)
        asdu.dir_pm = self.link_layer.dir_pm
        asdu.dir_registro = registro
        asdu.content = user_data
        asdu.generate()
        return asdu


class LinkLayer(metaclass=ABCMeta):

    def __init__(self, der=None, dir_pm=None):
        self.der = der
        self.dir_pm = dir_pm
        self._fcb = 0

    def initialize(self, physical_layer):
        self.physical_layer = physical_layer
        self.asdu_parser = FrameParser()

    def send_frame(self, frame):
        logger.info("sending frame\n {}".format(frame))
        logger.info("->" + ":".join("%02x" % b for b in frame.buffer))
        self.physical_layer.send_bytes(frame.buffer)

    def get_frame(self, timeout=30):
        frame = None
        logger.info("trying to receive frame")

        while frame is None:
            bt = self.physical_layer.get_byte(timeout)
            frame = self.asdu_parser.append_and_get_if_completed(bt)

        logger.info("received frame bytes {}".format(
            ":".join("%02x" % b for b in frame.buffer)))
        logger.info("received frame {}".format(frame))
        return frame

    def link_state_request(self):
        # es request response
        asdu = self.create_link_state_asdu()
        self.send_frame(asdu)
        resp = self.get_frame()
        if resp is None:
            raise ProtocolException("Link state request didn't get response")
        if resp.c.cf != 11:
            raise ProtocolException("Link state request no devolvio datos")

    def create_link_state_asdu(self):
        asdu = FixedFrame()
        asdu.c.res = 0
        asdu.c.prm = 1
        asdu.c.fcb = 0
        asdu.c.fcv = 0
        asdu.c.cf = 9
        asdu.der = self.der
        asdu.generate()
        return asdu

    def remote_link_reposition(self):
        # es send confirm
        # TODO estoy habría que comprobar errores en ack/nack
        # y repetir varias veces hasta que funcione
        resp = None
        asdu = self.create_remote_link_reposition_asdu()
        self.send_frame(asdu)
        resp = self.get_frame()
        if resp is None:
            raise ProtocolException("no answer received. maybe wrong der??")
        if resp.c.cf != 0:
            raise ProtocolException("Link reset no confirmó correctamente")

    def create_remote_link_reposition_asdu(self):
        """ se llama realmente la función Reset of remote link """
        asdu = FixedFrame()
        asdu.c.res = 0
        asdu.c.prm = 1
        asdu.c.fcb = 0
        asdu.c.fcv = 0
        asdu.c.cf = 0  # link state reposition
        asdu.der = self.der
        asdu.generate()
        return asdu

    @property
    def fcb(self):
        self._fcb += 1 % 2
        return self._fcb


class PhysicalLayer(metaclass=ABCMeta):

    @abstractmethod
    def send_byte(self, byte):
        pass

    def send_bytes(self, bts):
        for byte in bts:
            self.send_byte(byte)

    @abstractmethod
    def get_byte(self, timeout):
        pass
