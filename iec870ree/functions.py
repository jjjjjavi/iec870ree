from . import app_asdu
import logging
from abc import ABCMeta

logger = logging.getLogger('iec870ree.functions')


class NegativeConfirmationOfActivacion(Exception):
    pass


class IntegrationPeriodNotAvailable(Exception):
    pass


class RequestedASDUTypeNotAvailable(Exception):
    pass


class RequestedDataRecordNotAvailable(Exception):
    pass


class RequestedASDUTypeUnknownRM(Exception):
    pass


class Peticion(metaclass=ABCMeta):
    integrados = False

    def ejecutar(self, app_layer):
        app_layer.ejecutar_send_confirm(
            self.crear_user_data_asdu(), self.registro())
        while True:
            resp = app_layer.ejecutar_request_response()
            continuar = self.comprobar_causa_tm(resp)
            self.procesar_response(resp)
            if not self.integrados or not continuar:
                break
        return self.respuesta()

    def respuesta(self):
        return None

    def registro(self):
        return 0

    def obtener_user_data_asdu_class(self):
        if self.user_data_asdu_class:
            return self.user_data_asdu_class
        raise Exception("Clase mal configurada, "
                        "falta user_data_asdu o redefinir"
                        "crear_user_data_asdu")

    def crear_user_data_asdu(self):
        return self.obtener_user_data_asdu_class()()

    def procesar_response(self, resp):
        pass

    def comprobar_causa_tm(self, response):
        continuar = True
        if response.causa_tm == 0x05:
            logger.info("request or requested")
        elif response.causa_tm == 0x07:
            logger.info("activation confirmation actcon")
            if response.p_n == 1:
                raise NegativeConfirmationOfActivacion()
        elif response.causa_tm == 0x09:
            logger.info("deactivation confirmation")
        elif response.causa_tm == 0x0A:
            # si esperamos uno mas tendremos cf == 9, nack
            logger.info("activation termination actterm")
            continuar = False
        elif response.causa_tm == 0x0D:
            logger.error("requested data record not available")
            raise RequestedDataRecordNotAvailable
        elif response.causa_tm == 0x0E:
            logger.error("Requested ASDU-type not available")
            raise RequestedASDUTypeNotAvailable()
        elif response.causa_tm == 0x0F:
            logger.error("Requested ASDU desconocido")
            raise RequestedASDUTypeUnknownRM()
        elif response.causa_tm == 0x11:
            logger.error("Requested information object not available")
            raise IntegrationPeriodNotAvailable()
        elif response.causa_tm == 0x12:
            logger.error("Requested integration period not available")
            raise IntegrationPeriodNotAvailable()
        else:
            raise Exception('ERROR: Transmission cause unknown: {}'
                            .format(response.causa_tm))
        return continuar


class PeticionAutenticate(Peticion):

    def __init__(self, clave_pm):
        self.clave_pm = clave_pm

    def crear_user_data_asdu(self):
        return app_asdu.C_AC_NA_2(self.clave_pm)


class PeticionFinishSession(Peticion):
    user_data_asdu_class = app_asdu.C_FS_NA_2


class PeticionLeerTiempo(Peticion):
    user_data_asdu_class = app_asdu.C_TI_NA_2  # la respuesta es un M_TI_TA_2

    def procesar_response(self, resp):
        self.tiempo = resp.content.tiempo

    def respuesta(self):
        return self.tiempo.datetime


class PeticionGetInfo(Peticion):
    user_data_asdu_class = app_asdu.C_RD_NA_2  # la respuesta es un P_MP_NA_2

    def procesar_response(self, resp):
        self.valor = resp.content

    def respuesta(self):
        return self.valor


class PetiticionMultiplesDatosConRegistro(Peticion):
    # FIXME. Esto es un poco lioso, mejorar.
    integrados = True
    user_data_respuesta_asdu_class = None

    def __init__(self, start_date, end_date, direccion_registro):
        self.direccion_registro = direccion_registro
        self.start_date = start_date
        self.end_date = end_date
        self.valores = []

    def crear_user_data_asdu(self):
        return self.obtener_user_data_asdu_class()(self.start_date,
                                                   self.end_date)

    def registro(self):
        return self.direccion_registro.value

    def procesar_response(self, resp):
        if resp.tipo == self.user_data_respuesta_asdu_class.type:
            self.valores.append(resp.content)

    def respuesta(self):
        return self.valores


class PeticionDatosTarificacionActual(PetiticionMultiplesDatosConRegistro):
    # FIXME. Esto es un claro ejemplo de herencia de caja "blanca"
    user_data_asdu_class = app_asdu.C_TA_VC_2
    user_data_respuesta_asdu_class = app_asdu.M_TA_VC_2

    def __init__(self, direccion_registro):
        self.direccion_registro = direccion_registro
        self.valores = []

    def crear_user_data_asdu(self):
        return self.obtener_user_data_asdu_class()()


class PeticionLeerIncrementalValues(PetiticionMultiplesDatosConRegistro):
    user_data_asdu_class = app_asdu.C_CI_NU_2
    user_data_respuesta_asdu_class = app_asdu.M_IT_TK_2


class PeticionDatosTarificacionHistoricos(PetiticionMultiplesDatosConRegistro):
    user_data_asdu_class = app_asdu.C_TA_VM_2
    user_data_respuesta_asdu_class = app_asdu.M_TA_VM_2


class PeticionLeerAbsolutosValues(PetiticionMultiplesDatosConRegistro):
    user_data_asdu_class = app_asdu.C_CI_NT_2
    user_data_respuesta_asdu_class = app_asdu.M_IT_TG_2


class PeticionLeerBloqueIncrementalValues(PetiticionMultiplesDatosConRegistro):
    user_data_asdu_class = app_asdu.C_CB_UN_2
    user_data_respuesta_asdu_class = app_asdu.M_IB_TK_2

    def __init__(self, start_date, end_date,
                 direccion_registro, direccion_objeto):
        super().__init__(start_date, end_date, direccion_registro)
        self.direccion_objeto = direccion_objeto.value

    def crear_user_data_asdu(self):
        return self.obtener_user_data_asdu_class()(self.start_date,
                                                   self.end_date,
                                                   self.direccion_objeto)
