"""IP Physical Layer
"""
import logging
import queue
from .protocol import PhysicalLayer, PhysicalLayerTimeout
from .utils import PortReader, SocketPort

logger = logging.getLogger(__name__)


class Ip(PhysicalLayer):
    """IP Physical Layer"""

    def __init__(self, addr):
        """Create an IP Physical Layer.
        :addr tuple: Address tuple (host, port)
        """
        self.addr = addr
        self.connection = None
        self.queue = queue.Queue()
        self.thread = PortReader(self.queue)
        logger.debug("New IP with addr %s", addr)

    def connect(self):
        """Connect to `self.addr`
        """
        self.connection = SocketPort(self.addr)
        self.connection.connect()
        self.thread.port = self.connection
        self.thread.start()
        logger.debug("Connection with %s created", self.addr)

    def disconnect(self):
        """Disconnects
        """
        self.thread.stop()
        self.thread.join(5)
        logger.debug("disconnect")
        self.connection.disconnect()
        logger.debug("Disconnected from %s", self.addr)

    def send_byte(self, byte):
        """Send a byte"""
        self.connection.send_byte(byte)

    def get_byte(self, timeout=30):
        """Read a byte"""
        try:
            return self.queue.get(True, timeout=timeout)
        except queue.Empty:
            raise PhysicalLayerTimeout()
