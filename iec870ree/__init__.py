import logging
from .ip import Ip
from .modem import Modem
from .protocol import LinkLayer, AppLayer
from .functions import (
    PeticionAutenticate,
    PeticionFinishSession,
    PeticionLeerIncrementalValues,
)
from iec870ree.app_asdu import DireccionRegistro, DireccionObjeto

logger = logging.getLogger("iec870ree.iec870ree")


class Facade():

    def __init__(self, der, dir_pm, clave_pm):
        self.der = der
        self.dir_pm = dir_pm
        self.clave_pm = clave_pm
        self.inicializado = False

    def inicializar(self):
        self.physical_layer = self.obtener_physical_layer()
        self.link_layer = LinkLayer(self.der, self.dir_pm)
        self.link_layer.initialize(self.physical_layer)
        self.app_layer = AppLayer()
        self.app_layer.initialize(self.link_layer)
        self.physical_layer.connect()
        self.link_layer.link_state_request()
        self.link_layer.remote_link_reposition()
        PeticionAutenticate(self.clave_pm).ejecutar(self.app_layer)
        self.inicializado = True

    def obtener_medidas_horarias(self, fecha_desde, fecha_hasta):
        """ devuelve una lista de tuplas hora y consumo horario. """
        if not self.inicializado:
            self.inicializar()
        curva_horaria = PeticionLeerIncrementalValues(
            fecha_desde, fecha_hasta,
            DireccionRegistro.curva_de_carga).ejecutar(self.app_layer)
        resp = ((curva.tiempo.datetime, valor.total)
                for curva in curva_horaria
                for valor in curva
                if valor.address == DireccionObjeto.activa_entrante)
        return resp

    def obtener_medidas_cuarto_horarias(self, fecha_desde, fecha_hasta):
        """ devuelve una lista de tuplas hora y consumo cuarto horario. """
        if not self.inicializado:
            self.inicializar()
        curva_horaria = PeticionLeerIncrementalValues(
            fecha_desde, fecha_hasta,
            DireccionRegistro.curva_de_carga_cuartohoraria)\
            .ejecutar(self.app_layer)
        resp = ((curva.tiempo.datetime, valor.total)
                for curva in curva_horaria
                for valor in curva
                if valor.address == DireccionObjeto.activa_entrante)
        return resp

    def desconectar(self):
        try:
            PeticionFinishSession().ejecutar(self.app_layer)
        except Exception:
            # si falló en la conexión, esto puede ser
            # ruido...
            logger.info("Fallo al terminar session")
        finally:
            self.physical_layer.disconnect()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        try:
            self.desconectar()
        except Exception:
            logger.warning("Fallo al desconectar")
            if type is None:
                raise


class FacadeModem(Facade):

    def __init__(self, dispositivo, telefono, der, dir_pm, clave_pm):
        super().__init__(der, dir_pm, clave_pm)
        self.dispositivo = dispositivo
        self.telefono = telefono

    def obtener_physical_layer(self):
        return Modem(self.telefono, self.dispositivo)


class FacadeTcpIp(Facade):

    def __init__(self, ip, puerto, der, dir_pm, clave_pm):
        super().__init__(der, dir_pm, clave_pm)
        self.ip = ip
        self.puerto = puerto

    def obtener_physical_layer(self):
        return Ip((self.ip, self.puerto))
