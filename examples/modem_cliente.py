import sys
import logging
import datetime
import configparser
import getopt
from os.path import dirname, realpath, sep, pardir
library_path = dirname(realpath(__file__)) + sep + pardir
sys.path.append(library_path)
import iec870ree.modem # noqa
import iec870ree.protocol # noqa
from iec870ree.functions import ( # noqa
    PeticionAutenticate,
    PeticionFinishSession,
    PeticionLeerTiempo,
    PeticionLeerIncrementalValues,
    PeticionDatosTarificacionActual,
    PeticionDatosTarificacionHistoricos,
    PeticionLeerAbsolutosValues,
    PeticionLeerBloqueIncrementalValues,
    PeticionGetInfo,
)

from iec870ree.app_asdu import ( # noqa
    DireccionRegistro, DireccionObjeto,
)


logger = logging.getLogger('ejemplo.modem')


def run_example(port, phone_number, der, dir_pm, clave_pm):
    try:
        physical_layer = iec870ree.modem.Modem(phone_number, port)
        link_layer = iec870ree.protocol.LinkLayer(der, dir_pm)
        link_layer.initialize(physical_layer)
        app_layer = iec870ree.protocol.AppLayer()
        app_layer.initialize(link_layer)

        physical_layer.connect()
        link_layer.link_state_request()
        link_layer.remote_link_reposition()
        logger.info("before authentication")

        PeticionAutenticate(clave_pm).ejecutar(app_layer)

        # obtener hora aparato
        tiempo = PeticionLeerTiempo().ejecutar(app_layer)
        logger.info(tiempo)

        # obtener info aparato
        info = PeticionGetInfo().ejecutar(app_layer)
        logger.info(info)

        # obtener curva horaria aparato
        curva_horaria = PeticionLeerIncrementalValues(
            datetime.datetime(2019, 4, 1, 3, 0),
            datetime.datetime(2019, 4, 1, 5, 0),
            DireccionRegistro.curva_de_carga).ejecutar(app_layer)
        for curva in curva_horaria:
            logger.info(type(curva))
            logger.info(curva.tiempo.datetime)
            for valor in curva.valores:
                logger.info(valor)

        # obtener cierres aparato
        cierres = PeticionDatosTarificacionHistoricos(
            datetime.datetime(2019, 3, 1, 3, 0),
            datetime.datetime(2019, 4, 1, 5, 0),
            DireccionRegistro.tarificacion_primer_contrato).ejecutar(app_layer)
        logger.info("cierres {}".format(cierres))

        # obtener cierres aparato actuales
        cierres = PeticionDatosTarificacionActual(
            DireccionRegistro.tarificacion_primer_contrato).ejecutar(app_layer)
        logger.info("cierres {}".format(cierres))

        # obtener curva horaria absoluta aparato
        curva_horaria_abs = PeticionLeerAbsolutosValues(
            datetime.datetime(2019, 4, 1, 3, 0),
            datetime.datetime(2019, 4, 1, 5, 0),
            DireccionRegistro.curva_de_carga).ejecutar(app_layer)
        for curva in curva_horaria_abs:
            logger.info(type(curva))
            logger.info(curva.tiempo.datetime)
            for valor in curva.valores:
                logger.info(valor)

        # obtener curva horaria aparato activa
        curva_horaria = PeticionLeerBloqueIncrementalValues(
            datetime.datetime(2019, 4, 1, 3, 0),
            datetime.datetime(2019, 4, 1, 5, 0),
            DireccionRegistro.curva_de_carga,
            DireccionObjeto.generico_con_reservas).ejecutar(app_layer)
        for curva in curva_horaria:
            logger.info(type(curva))
            logger.info(curva.tiempo.datetime)
            for valor in curva.valores:
                logger.info(valor)

        PeticionFinishSession().ejecutar(app_layer)
    except:
        logger.exception("fallo en algo")
        raise
    finally:
        logger.info("ejecuto salida")
        physical_layer.disconnect()


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s-%(name)s-%(levelname)s:%(message)s',
        datefmt='%Y%m%d%H%M%S',
        level=logging.INFO)
    # logging.getLogger('iec870ree').setLevel(logging.WARNING)

    argv = sys.argv[1:]
    try:
        argv = sys.argv[1:]
        opts, args = getopt.getopt(argv, "hp:n:d:p:c:",
                                   ["port=", "phone_number=",
                                    "der=", "dir_pm=", "clave_pm="])
    except getopt.GetoptError:
        logger.error('wrong command')
        sys.exit(2)

    config = configparser.ConfigParser()
    config.read('config_modem.ini')
    for key in config['DEFAULT']:
        logger.info("key in config file {}".format(key))

    default = config['DEFAULT']

    port = default.get('port', "/dev/ttyUSB0")
    phone_number = default.get('phone_number', None)
    der = default.get('der', None)
    dir_pm = default.get('dir_pm', None)
    clave_pm = default.get('clave_pm', None)
    for opt, arg in opts:
        if opt == '-h':
            logger.error("help not implemented")
            sys.exit()
        elif opt in ("-p", "--port"):
            port = arg
        elif opt in ("-n", "--phone_number"):
            phone_number = arg
        elif opt in ("-d", "--der"):
            der = arg
        elif opt in ("-p", "--dir_pm"):
            dir_pm = arg
        elif opt in ("-c", "--clave_pm"):
            clave_pm = arg

    logger.info('Started {} {} {} {} {}'.format(port, phone_number,
                                                der, dir_pm, clave_pm))
    run_example(port, int(phone_number), int(der), int(dir_pm), int(clave_pm))
    logger.info("fin")
