import logging
from os.path import dirname, realpath, sep, pardir
import sys
import datetime
import configparser
library_path = dirname(realpath(__file__)) + sep + pardir
sys.path.append(library_path)
from iec870ree import FacadeTcpIp # noqa

logger = logging.getLogger('ejemplo.ip')


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s-%(filename)s:%(lineno)d-%(name)s-%(levelname)s"
        "%(message)s",
        datefmt='%Y%m%d%H%M%S',
        level=logging.INFO)
    logging.getLogger('iec870ree').setLevel(logging.WARNING)

    config = configparser.ConfigParser()
    config.read('config_ip.ini')
    for key, value in config['DEFAULT'].items():
        logger.info("key in config file {} {}".format(key, value))

    default = config['DEFAULT']

    ip = default.get('ip', None)
    port = default.get('port', None)
    der = default.get('der', None)
    dir_pm = default.get('dir_pm', None)
    clave_pm = default.get('clave_pm', None)

    facade = FacadeTcpIp(ip, port, int(der), int(dir_pm), int(clave_pm))
    with facade:
        medidas = facade.obtener_medidas_horarias(
            datetime.datetime.now() - datetime.timedelta(days=1),
            datetime.datetime.now(),
        )
        for medida in medidas:
            print("MEDIDA: ", medida)

        medidas = facade.obtener_medidas_cuarto_horarias(
            datetime.datetime.now() - datetime.timedelta(days=1),
            datetime.datetime.now(),
        )
        for medida in medidas:
            print("MEDIDA: ", medida)
