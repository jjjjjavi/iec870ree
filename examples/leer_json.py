import logging
import time
from os.path import dirname, realpath, sep, pardir
import sys
import datetime
import json
library_path = dirname(realpath(__file__)) + sep + pardir
sys.path.append(library_path)
from iec870ree import FacadeModem # noqa


logger = logging.getLogger('ejemplo.ip')
logging.basicConfig(
    format="%(asctime)s-%(filename)s:%(lineno)d-%(name)s-%(levelname)s"
    ":%(message)s",
    datefmt='%Y%m%d%H%M%S',
    level=logging.INFO)
logging.getLogger('iec870ree').setLevel(logging.WARNING)
# logging.getLogger('iec870ree.modem').setLevel(logging.DEBUG)


with open('config_modem.json', 'r') as myfile:
    data = myfile.read()

cupses = json.loads(data)
device = "/dev/ttyUSB0"

fecha_hasta = datetime.datetime.now()
fecha_desde = fecha_hasta - datetime.timedelta(hours=2)

for cups in cupses:
    # if cups.get('funciona', False):
    #     logger.info("Cups {} funciona. no pruebo".format(cups))
    #     continue

    logger.info("Probando: {} {}".format(cups, datetime.datetime.now()))
    try:
        facade = FacadeModem(device, cups['phone_number'],
                             int(cups['der']),
                             int(cups['dir_pm']), int(cups['clave_pm']))
        with facade:
            medidas = facade.obtener_medidas_horarias(fecha_desde,
                                                      fecha_hasta)
            for (hora, valor) in medidas:
                logger.info("medida {} {} {}".format(cups['cups'],
                                                     hora, valor))
    except Exception as e:
        # logger.exception("FALLA {} {} {}.".format(cups['cups'], type(e), e))
        logger.error("FALLA {} {} {}.".format(cups['cups'], type(e), e))
