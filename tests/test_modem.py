import unittest
import time
from iec870ree import modem
from iec870ree.utils import Port
import logging
from collections import OrderedDict
from unittest.mock import patch
logging.basicConfig(level=logging.DEBUG)


class TestModem(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.basicConfig(level=logging.DEBUG)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # @unittest.skip
    @patch('iec870ree.modem.SerialPort')
    def test_connect_disconnect(self, Serial):
        mock = MockSerialPort()
        Serial.return_value = mock
        phone_number = '636813395'
        m = modem.Modem(phone_number)
        logging.info(m)
        self.assertIsInstance(m.state, modem.DisconnectedState)
        m.connect()
        self.assertIsInstance(m.state, modem.DataState)
        m.disconnect()
        self.assertIsInstance(m.state, modem.DisconnectedState)

    # @unittest.skip
    @patch('iec870ree.modem.SerialPort')
    def test_get_byte(self, Serial):
        mock = MockSerialPort()
        Serial.return_value = mock
        phone_number = '636813395'
        with modem.Modem(phone_number) as m:
            m.connect()
            self.assertIsInstance(m.state, modem.DataState)
            time.sleep(1)
            bt = m.get_byte()
            self.assertEqual(bt, ord('A'))
            bt = m.get_byte()
            self.assertEqual(bt, ord('\n'))

    @patch('iec870ree.modem.SerialPort')
    def test_send_byte(self, Serial):
        mock = MockSerialPort()
        Serial.return_value = mock
        phone_number = '636813395'
        with modem.Modem(phone_number) as m:
            m.connect()
            self.assertIsInstance(m.state, modem.DataState)
            time.sleep(1)
            bt = b'A'[0]
            m.send_byte(bt)
            self.assertEqual(mock.sent_bytes_array, b'A')


class MockSerialPort(Port):

    def __init__(self):
        self.sent_bytes_array = bytearray()
        self.to_send = bytearray()

    def connect(self):
        pass

    def disconnect(self):
        pass

    def get_byte(self):
        time.sleep(0.01)
        if len(self.to_send) > 0:
            to_send = self.to_send[0]
            self.to_send = self.to_send[1:]
            return to_send

    def send_byte(self, bt):
        self.sent_bytes_array.append(bt)
        if bytes(self.sent_bytes_array) in MockSerialPort.RESPONSES_COMMAND_MODE:
            self.to_send += MockSerialPort.RESPONSES_COMMAND_MODE[bytes(self.sent_bytes_array)]
            logging.info("encontrado {} enviar {}".format(self.sent_bytes_array, self.to_send))
            self.sent_bytes_array = bytearray()

    RESPONSES_COMMAND_MODE = OrderedDict([
        (b'ATZ\x0d', bytearray(b'ATZ\x0d\x0a' + b'OK\x0d\x0a')),
        (b'AT+CBST=7,0,1\x0d', bytearray(b'AT+CBST=7,0,1\x0d\x0d\x0a' +
                                         b'OK\x0d\x0a')),
        (b'ATD636813395\x0d', bytearray(b'ATD636813395\x0d\x0d\x0a'
                                        + b'CONNECT 9600/RLP\x0d\x0a'
                                        + b'A\x0a')),
    ])
