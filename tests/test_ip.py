import unittest
from unittest.mock import patch, MagicMock
from iec870ree.ip import Ip
import logging


class TestIp(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.basicConfig(level=logging.DEBUG)

    def test_ip_phisical_initial_values(self):
        ip = Ip(('localhost', 20000))
        self.assertEqual(ip.addr, ('localhost', 20000))
        self.assertIsNone(ip.connection)

    @patch('iec870ree.ip.SocketPort')
    def test_connected(self, port):
        mc = port.return_value

        def get_byte():
            import time
            time.sleep(1)
            return None
        mc.get_byte.side_effect = get_byte
        ip = Ip(('example.org', 20000))
        ip.connect()
        self.assertIsInstance(ip.connection, MagicMock)
        mc.connect.assert_called_once()
        ip.disconnect()

    @patch('iec870ree.ip.SocketPort')
    def test_read_from_queue(self, port):
        mc = port.return_value

        mc.get_byte.side_effect = [
            1, 2, 3, 4, 5, 6
        ]
        ip = Ip(('example.org', 20000))
        ip.connect()
        self.assertIsInstance(ip.connection, MagicMock)
        self.assertEqual(ip.get_byte(), 1)
        self.assertEqual(ip.get_byte(), 2)
        ip.disconnect()
